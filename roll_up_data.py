from novi_data_sdk import NoviDataSdk
from pathlib import Path
import csv
from credentials import email, password

if __name__ == '__main__':
    novi_data = NoviDataSdk(
        email=email,
        password=password,
        version='v3', #rolling up of the data is only available in v3 currently
        scope='us-onshore',
        data_dir=Path(__file__).absolute().parent / 'data',
    )

    # example query: find oil and gas production by county, county where Year > 2010
    for data in novi_data.request('/wells', {
        'scope': 'us-onshore',
        'q[Year_gteq]': '2010',
        'agg_columns': "OilPerDay,GasPerDay",
        'agg_group_by_columns': 'State,County',
        'agg_function': 'SUM',
        'join_table': 'WellMonths'
    }):
        # do something with the data here…
        print(data)

    # example query: find oil and gas production by Date where Year > 2010
    # when grouping data only on columns in WellMonths we dont need to join with Wells
    for data in novi_data.request('/well_months', {
        'scope': 'us-onshore',
        'q[Year_gteq]': '2010',
        'agg_columns': "OilPerDay,GasPerDay",
        'agg_group_by_columns': 'Date',
        'agg_function': 'SUM',
    }):
        # do something with the data here…
        print(data)

    #example query to get permit count by state
    for data in novi_data.request('/well_permits', {
        'scope': 'us-horizontals',
        'agg_columns': "API10",
        'agg_group_by_columns': 'State',
        'agg_function': 'COUNT',
        'join_table': 'Wells'
    }):
        print(data)



