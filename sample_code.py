from novi_data_sdk import NoviDataSdk
from pathlib import Path
import csv
from credentials import email, password

if __name__ == '__main__':
    novi_data = NoviDataSdk(
        email=email,
        password=password,
        version='v3',            # supported values: 'v2', 'v3'
        scope='us-horizontals',  # supported values: 'us-horizontals', 'gom', 'us-onshore'
        data_dir=Path(__file__).absolute().parent / 'data',
    )

    # example query: retrieve and print all basins
    print(list(
        novi_data.request('/basins')
    ))

    # example query: retrieve all wells in Texas with spud date >= 2021-02-01
    for well in novi_data.request('/wells', {
        'q[State_eq]': 'Texas',
        'q[SpudDate_gteq]': '2021-02-01',
    }):
        # do something with the data here…
        print(well)

    # example query: retrieve all well months of onshore wells by Operator Exxon Mobil and Year >= 2020
    for well_month in novi_data.request('/well_months', {
        'q[Operator_eq]': 'Exxon Mobil',
        'q[Year_gteq]': '2020',
        'scope': 'us-onshore',  # you can overwrite the scope on a request basis
    }):
        # do something with the data here…
        print(well_month)

    # example query: retrieve details of well with API10=4247944400
    well = novi_data.request_one('/wells', {
        'q[API10_eq]': '4247944400'
    })
    # do something with well data here…
    print(well)
    for well_month in novi_data.request('/well_months', {
        'q[API10_eq]': well['API10']
    }):
        # do something with the well months data here…
        print(well_month)

    # example bulk download: fetch all latest data
    bulk_all_dir = novi_data.update_bulk_data()
    print('Bulk data is available in:', bulk_all_dir)
    # you can now use it, for example with a csv reader:
    with open(bulk_all_dir / 'Database' / 'Wells.tsv', 'r', encoding='utf-8') as csvfile:
        for well in csv.DictReader(csvfile, delimiter='\t'):
             print(well)
    # or with pandas:
    wells_df = pd.read_csv(bulk_all_dir / 'Database' / 'Wells.tsv')

    # example bulk download: fetch latest onshore Permian data
    bulk_onshore_permian_dir = novi_data.update_bulk_data(scope='us-onshore', basin='Permian')
    print('Bulk data is available in:', bulk_onshore_permian_dir)
    # usage as above

    # example bulk download: fetch latest Delaware data
    bulk_onshore_permian_dir = novi_data.update_bulk_data(subbasin='Delaware')
    print('Bulk data is available in:', bulk_onshore_permian_dir)
    # usage as above
