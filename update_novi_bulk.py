from pathlib import Path
from getpass import getpass
from typing import List, Dict, Union, Tuple
from novi_data_sdk import NoviDataSdk, NoviDataAuthenticationException
from argparse import ArgumentParser


project_dir = Path(__file__).absolute().parent
credentials_file = project_dir / 'credentials.py'
data_dir = project_dir / 'data'


VERSIONS = ['v1', 'v2', 'v3']
SCOPES = ['us-horizontals', 'us-onshore', 'gom']


def escape_single_quote(s: str) -> str:
    return s.replace("'", '\\')


def input_credentials():
    print('Please provide credentials…')

    email = input('Email: ').strip()
    password = getpass('Password: ').strip()

    try:
        NoviDataSdk(
            email=email,
            password=password,
            data_dir=data_dir,
        )
    except NoviDataAuthenticationException as e:
        print('Invalid credentials, please try again…')
        return False

    with open(credentials_file, 'w+') as f:
        f.write(f'''
# this file should be kept confidential and not be committed to a repository

email = '{escape_single_quote(email)}'
password = '{escape_single_quote(password)}'
''')
        print(f'Authorization successful. Credentials have been saved to: {credentials_file}')
        return True


def select(prompt: str, options: Union[List[str], Dict[str, any]], default = None):
    if type(options) == list:
        options = dict(zip(options, options))

    print(prompt)
    for i, option in enumerate(options.keys(), start=1):
        print(f'   {str(i).rjust(3, " ")}: {option}')

    while True:
        selection = input('Selection: ' if default is None else f'Selection [{default}]: ').strip()
        if not selection and default:
            return options[default]

        if selection in options.keys():
            return selection

        try:
            return options[list(options.keys())[int(selection) - 1]]
        except ValueError:
           pass
        except IndexError:
           pass


def build_list_of_downloadable_basins(raw_basins: List[dict]) -> Dict[str, Tuple[str, str]]:
    basins_subbasins = {}
    for row in raw_basins:
        if row['Basin'] not in basins_subbasins:
            basins_subbasins[row['Basin']] = []
        if 'HasSubbasinBulkFile' in row and row['HasSubbasinBulkFile']:
            basins_subbasins[row['Basin']].append(row['Subbasin'])

    options = {}
    options['All basins'] = ('', '')
    for basin, subbasins in basins_subbasins.items():
        if len(subbasins):
            options[f'{basin} - All subbasins'] = (basin, '')
            for subbasin in subbasins:
                options[f'{basin} - {subbasin}'] = (basin, subbasin)
        else:
            options[basin] = (basin, '')

    return options


if __name__ == '__main__':
    parser = ArgumentParser('Novi Data – bulk downloader')
    parser.add_argument('-v', '--version')
    parser.add_argument('-s', '--scope')
    parser.add_argument('-b', '--basin')
    parser.add_argument('-f', '--no-diffs', default=False, nargs='?', const=True)

    args = parser.parse_args()

    print('Novi Data – bulk downloader')

    while not credentials_file.exists():
        input_credentials()

    from credentials import email, password

    version = args.version or select('Which API version should be used?', VERSIONS, 'v3')
    scope = args.scope or select('Which scope should be used?', SCOPES, 'us-horizontals')

    novi_data = NoviDataSdk(
        email=email,
        password=password,
        version=version,
        scope=scope,
        data_dir=data_dir,
    )

    basins = build_list_of_downloadable_basins(novi_data.request('/basins'))
    basin, subbasin = basins[args.basin.replace('–', '-')] \
        if args.basin \
        else select('Which basin/subbasin to download?', basins, 'All basins')

    bulk_target_dir = novi_data.update_bulk_data(basin=basin, subbasin=subbasin, no_diffs=args.no_diffs)
    print('Download complete, bulk data is available in:', bulk_target_dir)
