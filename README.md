# Novi Insight Engine REST API sample code

## API documentation

You can find detailed documentation [here](https://insight.novilabs.com/apidocs)

## Using an update script (no code)

We've prepared a script that guides you through the process of collecting required parameters
and then ensures that the latest available version of the data you requested is present in the `/data` folder.

Open the terminal or command prompt, `cd` into the `rest-api-sample-code` folder, and run:

```shell
python ./update_novi_bulk.py
```

You can also specify params directly in the command line (useful for automation): 

```shell
py ./update_novi_bulk.py --version=v3 --scope=us-horizontals --basin="Uinta" --basin="Permian – All subbasins"
```

It is recommended to regularly (e.g. weekly) download the full dataset by using the `--no-diffs` flag.

```shell
py ./update_novi_bulk.py --no-diffs
```


## Using the SDK (code)

Create a file `credentials.py` using the template below. Make sure it's kept confidential and isn't committed to the repository.

```python
email = 'person@example.com'
password = 'my-password'
```

File `novi_data_sdk.py` contains a library that exposes a simple interface to communicate with the REST API.
You can ignore it, if you're not interested in implementation details.

File `sample_code.py` shows how to use that library. Please read it and adjust it to your needs.

In order to execute it, open the terminal or command prompt, `cd` into the `rest-api-sample-code` folder, and run:

```shell
python ./sample_code.py
```

## Database diffs (introduced in v3)

It's now possible to optimise fetching the data – instead of downloading the full data set every time,
you can just fetch all the incremental changes that happened since the last version you have.

If you're using the update script, diffs will be used automatically
(script will fall back to fetching the full data set if necessary or more optimal).

If you're writing the code yourself: instead of calling `/bulk` and then fetching the zip file from the `URL` field,
call `/bulk-diffs?since=2023-08-08T10:08:43.000Z` instead (replace the timestamp with the latest version you have;
you can see it in the `ExportDate.txt` file), iterate over the fields in the response and fetch data in `DiffURL`.
Apply the resulting files to your database in the given order.

The format of the diff files is the same as regular exports, except it only contains the rows that have been
added, modified, or soft-deleted since the previous export.

Note: shapefiles are not included in the diff bulk file. If you want to keep them up to date,
you can fetch them separately from the database using the `ShapefileURL` field.

## Loading data to a database 

We provide a database schema for postgres, you can load it with the following command:

```bash
psql -U {your_username} -d {your_database_name} < schema.postgres.sql
```

To populate the newly created tables with data from the TSV files, you can use the postgres `COPY` command like this:

```SQL
COPY "Organizations" FROM '/path/to/Organizations.tsv' WITH (FORMAT CSV, DELIMITER E'\t', HEADER);
```

If you want to apply a diff file to an existing database table, a suggested approach could be to the following:

```SQL
BEGIN;

-- create a temporary table with the same schema as the full table
CREATE TEMPORARY TABLE "Organizations_diff" (
    "Id" int4 NOT NULL,
    "Company" varchar(64) NOT NULL,
    "Ticker" varchar(8) NULL,
    "IsPublic" bool NOT NULL,
    "URL" varchar(255) NULL,
    "CreatedAt" timestamp NULL,
    "ModifiedAt" timestamp NULL,
    "DeletedAt" timestamp NULL,
    CONSTRAINT "Organizations_diff_pkey" PRIMARY KEY ("Id")
);

-- load the diff file to the temporary diff table
COPY "Organizations_diff" FROM '/path/to/Organizations--diff.tsv' WITH (FORMAT CSV, DELIMITER E'\t', HEADER);

-- remove the rows that the diff provides a new version for
DELETE FROM "Organizations" WHERE "Id" in (SELECT "Id" FROM "Organizations_diff");

-- copy all data from the temporary table to the full table
INSERT INTO "Organizations" SELECT * FROM "Organizations_diff";

-- clean up the temporary table
DROP TABLE "Organizations_diff";

COMMIT;
```


## Rolling up data (introduced in v3)

It's now possible to roll up data in the API. for example, it's now
possible to fetch
1. Total Oil and Gas production by state or county level or by date
2. Total permit count by state
3. Count of first producing wells, completed wells, spudded wells


##  Details on how to access
1. All the end points now accept the following parameters
    - `agg_function`: Name of the `Aggregate` function you want to
      apply .e.g. `SUM, COUNT, MAX, MIN`
    - `agg_columns`: Specifiy here comma separated column names on which the aggregate
      function should be applied. e.g. `OilPerDay,GasPerDay`
    - `agg_group_by_columns`: Specify here the comma separated list of
      column names by which the results should be grouped. e.g. `State,
County, Date`
    - `join_table`: Specify here the table name with which you would like
      to perform a `JOIN`. for e.g. to get state and county level
production data, we would need to join WellMonths and Wells. when
requesting `well_months` end point, you can specify here "Wells". this
will perform a JOIN between the WellMonths and Wells table

2. Please note that the name of the columns to be passed in the above
   parameters are **case sensitive**. Detailed list of column names and
table names can be found under `Database Schema`  <a
href='https://insight.novilabs.com/apidocs'>here</a>

3. Example on how to call the end points can be found in the roll_up_data.py

## Additional points
1. Try to use as many filters as possible when rolling up data to avoid
   any timeout issues. for example, when requesting State / County level
production data, please user filters like `q[Year_gteq] > 2010` or
`q[Year_eq] = 2000`, `q[Basin_eq] = Permian` etc.
2. The table or column names passed in the paramater list is
   case-sensitive for example, if you want to join with wells table
please pass the table names as listed here for the us-onshore scope <a href="https://schema-documentation.s3.amazonaws.com/Database_description_US_Onshore_T1_v3.pdfhttps://schema-documentation.s3.amazonaws.com/Database_description_US_Onshore_T1_v3.pdf">here</a>
3. The column names which you can pass can be found <a href="https://schema-documentation.s3.amazonaws.com/Database_description_US_Onshore_T1_v3.pdf">here</a>. On this link you can click
on any table name under Table definations, this will should you all the
column names you can pass to the end points. for example `OilPerDay`,
`GasPerDay` or `Date`
4. Rolling up requests might take a couple of minutes to process when
   data involves large amount of rows
